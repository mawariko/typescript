export interface IIndexable {
  [key: string]: any;
}

export interface BasicFighter extends IIndexable {
  _id: string;
  name: string;
  source: string;
}

export interface Fighter extends BasicFighter {
  health: number;
  attack: number;
  defense: number;
}

export enum HttpMethod {
  get = 'GET',
  post = 'POST',
  put = 'PUT',
  delete = 'DELETE',
}
