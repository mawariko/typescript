import { callApi } from '../helpers/apiHelper';
import { BasicFighter, Fighter, HttpMethod } from '../types';

class FighterService {
  async getFighters(): Promise<BasicFighter[]> {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, HttpMethod.get);

      return apiResult as BasicFighter[];
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<Fighter> {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, HttpMethod.get);

      return apiResult as Fighter;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
