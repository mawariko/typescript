import { controls } from '../../constants/controls';
import { Fighter } from 'types';

enum Attacker {
  first,
  second,
}

enum AttackTypes {
  simple,
  blocked,
  super,
}

enum FightClasses {
  damage = 'damage',
  block = 'block',
  attack = 'attack',
}

export async function fight(firstFighter: Fighter, secondFighter: Fighter): Promise<Fighter> {
  return new Promise((resolve) => {
    const initHealth1 = firstFighter.health;
    const initHealth2 = secondFighter.health;
    const keys: Set<string> = new Set();
    const firstAttacker = Attacker.first;
    const secondAttacker = Attacker.second;
    const firstAttackerImgbox = document.querySelector('.arena___left-fighter')!;
    const secondAttackerImgbox = document.querySelector('.arena___right-fighter')!;
    const firstFighterBar = document.getElementById('left-fighter-indicator')!;
    const secondFighterBar = document.getElementById('right-fighter-indicator')!;
    const arena = document.querySelector('.arena___battlefield')!;

    const afterAttackPause = 10000;

    const blockFighter = {
      first: false,
      second: false,
    };

    window.addEventListener('keydown', keysPressed, false);
    window.addEventListener('keyup', keysReleased, false);

    function keysPressed(e: KeyboardEvent): void {
      keys.add(e.code);
      keycombo();
    }

    function keysReleased(e: KeyboardEvent): void {
      keys.delete(e.code);
    }

    function keycombo(): void {
      switch (true) {
        case superCombo(controls.PlayerOneCriticalHitCombination) && !blockFighter.first:
          updateHealth(firstFighter, secondFighter, secondFighterBar, AttackTypes.super, initHealth2);
          showMove(firstAttacker, FightClasses.damage);
          delay(firstAttacker);
          break;
        case superCombo(controls.PlayerTwoCriticalHitCombination) && !blockFighter.second:
          updateHealth(secondFighter, firstFighter, firstFighterBar, AttackTypes.super, initHealth1);
          delay(secondAttacker);
          showMove(secondAttacker, FightClasses.damage);
          break;
        case superCombo([controls.PlayerOneAttack, controls.PlayerTwoBlock]):
          updateHealth(firstFighter, secondFighter, secondFighterBar, AttackTypes.blocked, initHealth2);
          showMove(firstAttacker, FightClasses.block);
          break;
        case superCombo([controls.PlayerTwoAttack, controls.PlayerOneBlock]):
          updateHealth(secondFighter, firstFighter, firstFighterBar, AttackTypes.blocked, initHealth1);
          showMove(secondAttacker, FightClasses.block);
          break;
        case keys.has(controls.PlayerOneAttack):
          updateHealth(firstFighter, secondFighter, secondFighterBar, AttackTypes.simple, initHealth2);
          showMove(firstAttacker, FightClasses.damage);
          break;
        case keys.has(controls.PlayerTwoAttack):
          updateHealth(secondFighter, firstFighter, firstFighterBar, AttackTypes.simple, initHealth1);
          showMove(secondAttacker, FightClasses.damage);
          break;
      }

      const winner =
        isAlive(firstFighter) && isAlive(secondFighter) ? null : !isAlive(firstFighter) ? secondFighter : firstFighter;

      if (winner) {
        winner == firstFighter ? (secondFighterBar.style.width = '0') : (firstFighterBar.style.width = '0');
        window.removeEventListener('keydown', keysPressed, false);
        window.removeEventListener('keyup', keysReleased, false);
        resolve(winner);
      }
    }

    //  FUNCTION
    function superCombo(combo: string[]): boolean {
      return combo.every((key) => keys.has(key));
    }

    // check if the fighter is alive && is winner
    function isAlive(fighter: Fighter): number {
      return (fighter.health = fighter.health > 0 ? fighter.health : 0);
    }

    //fight visualization
    function showMove(fighter: Attacker, showClass: FightClasses): void {
      let opponentBox = fighter == Attacker.first ? secondAttackerImgbox : firstAttackerImgbox;
      arena.classList.add(FightClasses.attack);
      opponentBox.classList.add(showClass);
      setTimeout(() => {
        opponentBox.classList.remove(showClass);
        arena.classList.remove(FightClasses.attack);
      }, 300);
    }

    // delay after super attack
    function delay(attacker: Attacker): void {
      attacker == Attacker.first ? (blockFighter.first = true) : (blockFighter.second = true);

      setTimeout(() => {
        attacker == Attacker.first ? (blockFighter.first = false) : (blockFighter.second = false);
      }, afterAttackPause);
    }
  });
}

// key function
function updateHealth(
  attacker: Fighter,
  defender: Fighter,
  bar: HTMLElement,
  type: AttackTypes,
  initHealth: number
): number {
  const damage = getDamage(type, attacker, defender);
  defender.health -= damage;
  const damagePercent = Math.floor((defender.health! / initHealth) * 100);
  bar.style.width = `${damagePercent}%`;
  return defender.health;
}

// calculates damage from 3 possible attacks: simple, simple + opponent is blocking, super.

function getDamage(type: AttackTypes, attacker: Fighter, defender: Fighter): number {
  switch (type) {
    case AttackTypes.simple:
      return unblockedDamage(attacker);
    case AttackTypes.blocked:
      return blockedDamage(attacker, defender);
    case AttackTypes.super:
      return superDamage(attacker);
  }
}

function blockedDamage(attacker: Fighter, defender: Fighter): number {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const damage = Math.max(0, hitPower - blockPower);
  return damage;
}

function unblockedDamage(attacker: Fighter): number {
  const damage = getHitPower(attacker);
  return damage;
}

function superDamage(attacker: Fighter): number {
  const damage = 2 * getHitPower(attacker);
  return damage;
}

//  Helper functions for get damage

export function getHitPower(fighter: Fighter): number {
  const criticalHitChance = Math.random() + 1;
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter: Fighter): number {
  const dodgeChance = Math.random() + 1;
  const block = fighter.defense * dodgeChance;
  return block;
}
