import {
  showModal
} from './modal'
import {
  createElement
} from '../../helpers/domHelper'
import {
  createImage
} from '../fightersView'
import { Fighter } from 'types';

export function showWinnerModal(fighter: Fighter) : void {
  const bodyElementWinner = createElement({
    tagName: 'div',
    className: 'winner-holder'
  })
  const elementWinnerText = createElement({
    tagName: 'div',
    className: 'winner-modal__text'
  })

  elementWinnerText.textContent = `Congratulations, fighter! See you in the next round!`
  const winnerImage = createImage(fighter)

  bodyElementWinner.append(winnerImage, elementWinnerText)
  showModal({
    title: `Well done, ${fighter.name}`,
    bodyElement: bodyElementWinner,
    onClose
  })
}

// restart the app
function onClose() : void {
  location.reload(); 
}