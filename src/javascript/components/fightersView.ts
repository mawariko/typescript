import { createElement } from '../helpers/domHelper';
import { createFightersSelector, FighterSelectorCallback } from './fighterSelector';
import { BasicFighter, Fighter } from 'types';

export function createFighters(fighters: BasicFighter[]): HTMLDivElement {
  const selectFighter: FighterSelectorCallback = createFightersSelector();
  const container = createElement({ tagName: 'div', className: 'fighters___root' });
  const preview = createElement({ tagName: 'div', className: 'preview-container___root' });
  const fightersList = createElement({ tagName: 'div', className: 'fighters___list' });
  const fighterElements = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container as HTMLDivElement;
}

function createFighter(fighter: BasicFighter, selectFighter: FighterSelectorCallback): HTMLDivElement {
  const fighterElement = createElement({ tagName: 'div', className: 'fighters___fighter' });
  const imageElement = createImage(fighter);
  const onClick = (event: Event) => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement as HTMLDivElement;
}

export function createImage(fighter: BasicFighter): HTMLImageElement {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes,
  });

  return imgElement as HTMLImageElement;
}
