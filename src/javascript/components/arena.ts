import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import {fight} from './fight'
import { showWinnerModal } from './modal/winner';
// ------------
import { Fighter } from 'types';

enum Position {
  left = 'left',
  right = 'right'
}
// ------------
export function renderArena(selectedFighters: [Fighter, Fighter]): void {
  // -------------
  const root = document.getElementById('root')!;
  const arena = createArena(selectedFighters);
  root.innerHTML = '';
  root.append(arena);
  // - start the fight
  fight(...selectedFighters)
  // - when fight is finished show winner
  // -----------
  .then((winner: Fighter) => {showWinnerModal(winner)});
}

// -------------
function createArena(selectedFighters: [Fighter, Fighter]): HTMLDivElement {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(...selectedFighters);
  const fighters = createFighters(...selectedFighters);
  
  arena.append(healthIndicators, fighters);
  // --------------
  return arena as HTMLDivElement;
}

function createHealthIndicators(leftFighter: Fighter, rightFighter: Fighter): HTMLDivElement {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, Position.left);
  const rightFighterIndicator = createHealthIndicator(rightFighter, Position.right);

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators as HTMLDivElement;
}

function createHealthIndicator(fighter: Fighter, position: Position): HTMLDivElement {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container as HTMLDivElement;
}

function createFighters(firstFighter: Fighter, secondFighter: Fighter): HTMLDivElement{
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, Position.left);
  const secondFighterElement = createFighter(secondFighter, Position.right);

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField as HTMLDivElement;
}

function createFighter(fighter: Fighter, position: string): HTMLDivElement {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === Position.right ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement as HTMLDivElement;
}
