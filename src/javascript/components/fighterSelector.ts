import { createElement } from '../helpers/domHelper';
import { fighterService } from '../services/fightersService';
import { renderArena } from './arena';
import * as versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { Fighter } from 'types';

export type FighterSelectorCallback = (event: Event, fighterId: string) => Promise<void>;

export function createFightersSelector(): FighterSelectorCallback {
  let selectedFighters: Fighter[] = [];

  return async (event: Event, fighterId: string) => {
    const fighter: Fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = (playerOne !== null && playerOne !== undefined) ?
      playerOne:
      fighter;
    const secondFighter = Boolean(playerOne) ?
     (playerTwo !== null && playerTwo !== undefined)? playerTwo : fighter 
     : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map();


export async function getFighterInfo(fighterId: string): Promise<Fighter> {
  let fighterInfo: Fighter;
  if(!fighterDetailsMap.has(fighterId)) {
    fighterInfo = await fighterService.getFighterDetails(fighterId);
    fighterDetailsMap.set(fighterId, fighterInfo)
  } else {
    fighterInfo = fighterDetailsMap.get(fighterId);
  }

  return fighterInfo;
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
}

function renderSelectedFighters(selectedFighters: Fighter[]) {
  const fightersPreview = document.querySelector('.preview-container___root')!;
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, 'left');
  const secondPreview = createFighterPreview(playerTwo, 'right');
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: Fighter[]): HTMLDivElement {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters as [Fighter, Fighter]);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn: string = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container as HTMLDivElement;
}

function startFight(selectedFighters: [Fighter, Fighter]) {
  renderArena(selectedFighters);
}
